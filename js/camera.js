"use strict";

function CameraFactory() {

    this.availableCameras = function( onAvailableCameraFn ) {

        if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
            console.log("Getting devices through navigator.mediaDevices.enumerateDevices.");
            navigator.mediaDevices.enumerateDevices()
                .then(function(devices) {
                    var options = [];
                    devices.forEach(function(device) {
                        if(device.kind == "videoinput"){
                            options.push({
                                facing: device.facing,
                                id: device.deviceId,
                                kind: device.kind,
                                label: device.label
                            });
                        }
                    });
                    onAvailableCameraFn(options);
                })
                .catch(function(err) {
                    console.log("Fallbacking to default camera." + err.name + ": " + err.message);
                    // timeout=0 is here to push the callback execution at the end of the event queue,
                    // thus avoiding the callback fn() to be invoked before returning.
                    window.setTimeout(
                        function(){
                            onAvailableCameraFn([{
                                facing: 'default',
                                id: 'default',
                                kind: 'default',
                                label: 'default'
                            }])
                        },
                        0);
                });
            return;
        }

        try {

            MediaStreamTrack.getSources(function (sourceInfos) {

                var options = [];

                for (var i = 0; i != sourceInfos.length; ++i) {
                    var sourceInfo = sourceInfos[i];
                    if (sourceInfo.kind === 'video') {
                        console.log(sourceInfo.id, sourceInfo.label || 'camera');
                        options.push({
                            facing: sourceInfo.facing,
                            id: sourceInfo.id,
                            kind: sourceInfo.kind,
                            label: sourceInfo.label
                        });
                    }
                }

                onAvailableCameraFn(options);
            });

        }catch(err){
            console.log("Fallbacking to default camera." + err.name + ": " + err.message);
            // timeout=0 is here to push the callback execution at the end of the event queue,
            // thus avoiding the callback fn() to be invoked before returning.
            window.setTimeout(
                function(){
                    onAvailableCameraFn([{
                        facing: 'default',
                        id: 'default',
                        kind: 'default',
                        label: 'default'
                    }])
                },
                0);
        }
    }

    this.createCamera = function (videoTag, constraint) {

        constraint = constraint || null;

        var camera = null;

        var reasons = [];

        if (camera == null && navigator.mediaDevices && navigator.mediaDevices.getUserMedia) { // Not yet used new standard
            return new StandardCamera(videoTag, constraint);
        }else if(camera == null && navigator.getUserMedia) { // Deprecated old standard
            reasons.push("Mode 'navigator.getUserMedia' not yet supported.");
        } else if(camera == null && navigator.webkitGetUserMedia) { // WebKit-prefixed
            camera = new WebkitCamera(videoTag, constraint);
        } else if(camera == null && navigator.mozGetUserMedia) { // Mozilla-prefixed
            reasons.push("Mode 'navigator.mozGetUserMedia' not yet supported.");
        } else {
            reasons.push("Your browser is not supported.");
        }

        if(camera == null){
            throw "Unable to get Camera. Reasons: " + reasons.join();
        }

        return camera;

    }

    // Creates an object that acts like a camera but does nothing.
    this.createFakeCamera = function(){
        return new FakeCamera();
    }

}

function FakeCamera() {
    this.turnOn = function(errBack) {

    }

    this.turnOff = function() {

    }
}

function WebkitCamera(videoTag, constraint) {

    this._videoTag = videoTag;
    this._cameraStreamUrl = null;
    this._cameraStream = null;
    var outer = this;

    this.turnOn = function(errBack) {

        var videoPreferences = constraint == null ? {video: { facingMode: ("environment") } } : constraint;

        navigator.webkitGetUserMedia(videoPreferences, function(stream){

            outer._cameraStream = stream;
            try {
                outer._videoTag.srcObject = stream;
            } catch (error) {
                outer._cameraStreamUrl = window.URL.createObjectURL(outer._cameraStream);
                outer._videoTag.src = outer._cameraStreamUrl;
            }

            // on mobile it raises: VM48 app.js:101Uncaught (in promise) DOMException: play() can only be initiated by a user gesture.
            // on desktop works
            // video.play();

        }, errBack);

    }

    this.turnOff = function() {
        outer._videoTag.pause();

        outer._cameraStream.enabled=false;
        try{
            outer._cameraStream.stop();
        }catch(e){
            console.log(e);
        }
        try{
                                // without args: TypeError: outer._cameraStream.stop is not a function
            var arg = false;    // TypeError: Failed to execute 'removeTrack' on 'MediaStream': parameter 1 is not of type 'MediaStreamTrack'.
            var tracks = outer._cameraStream.getTracks();
            tracks.forEach( function(track){
                outer._cameraStream.removeTrack(track);
            });
        }catch(e){
            console.log(e);
        }
        if(outer._cameraStreamUrl) {
            window.URL.revokeObjectURL(outer._cameraStreamUrl);
        }
    }

}

function StandardCamera(videoTag, constraint) {

    this._videoTag = videoTag;
    this._cameraStreamUrl = null;
    this._cameraStream = null;
    var outer = this;

    this.turnOn = function (onErrorFn) {

        if (typeof onErrorFn == 'undefined') {
            onErrorFn = function(err){
                console.log("Error:" + err);
            }
        }

        var videoPreferences = constraint == null ? {video: { facingMode: ("environment") } } : constraint;

        // https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
        navigator
            .mediaDevices
            .getUserMedia(videoPreferences)
            .then(function (stream) {
                outer._cameraStream = stream;
                try {
                    outer._videoTag.srcObject = stream;
                } catch (error) {
                    outer._cameraStreamUrl = window.URL.createObjectURL(outer._cameraStream);
                    outer._videoTag.src = outer._cameraStreamUrl;
                }    
                try{
                    videoTag.play();
                }catch(e){

                }
            }).catch(onErrorFn);

    }

    this.turnOff = function() {
        outer._videoTag.pause();



        if(outer._cameraStreamUrl) {
            window.URL.revokeObjectURL(outer._cameraStreamUrl);
        }
    }

}

// function ReferenceWebkitCamera(_videoTag) {
//
//     this._videoTag = _videoTag;
//
//     this.turnOn = function(errBack) {
//
//         var videoPreferences = {video: { facingMode: ("environment") } };
//
//         // Get access to the camera!
//         if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
//             navigator.mediaDevices.getUserMedia({video: true}).then(function (stream) {
//                 videoTag.src = window.URL.createObjectURL(stream);
//                 videoTag.play();
//             });
//         }else if(navigator.getUserMedia) { // Standard
//             navigator.getUserMedia(videoPreferences, function(stream) {
//                 videoTag.src = stream;
//                 videoTag.play();
//             }, errBack);
//         } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
//             navigator.webkitGetUserMedia(videoPreferences, function(stream){
//
//                 // on both mobile and browser: 'webkitURL' is deprecated. Please use 'URL' instead.
//                 $scope.theStream = window.URL.createObjectURL(stream);
//                 videoTag.src = $scope.theStream;
//
//                 // on mobile it raises: VM48 app.js:101Uncaught (in promise) DOMException: play() can only be initiated by a user gesture.
//                 // on desktop works
//                 // video.play();
//
//             }, errBack);
//         } else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
//             navigator.mozGetUserMedia(videoPreferences, function(stream){
//                 videoTag.src = window.URL.createObjectURL(stream);
//                 videoTag.play();
//             }, errBack);
//         }
//     }
//
//     this.turnOff = function() {
//         // Grab elements, create settings, etc.
//         var video = document.getElementById('video');
//
//         var errBack = function (e) {
//             console.log(e);
//         }
//
//         // Get access to the camera!
//         if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
//             video.pause();
//         }else if(navigator.getUserMedia) { // Standard
//             video.pause();
//         } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
//             video.pause();
//             console.log($scope.theStream);
//             window.URL.revokeObjectURL($scope.theStream);
//         } else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
//             video.pause();
//         }
//     }
//
//     this.takePhoto = function () {
//         var canvas = document.getElementById('canvas');
//         var context = canvas.getContext('2d');
//         var video = document.getElementById('video');
//
//         context.drawImage(video, 0, 0, 320, 200);
//
//     }
//
//
// }


function takePhoto() {

    // gets the video
    var video = window.document.getElementById('video');

    // create a throw away detached canvas with the sole scope
    // to store the image from the camera
    var canvas = window.document.createElement('canvas');
    canvas.setAttribute('width', "" + video.videoWidth);
    canvas.setAttribute('height', "" + video.videoHeight);
    var context = canvas.getContext('2d');

    // turn off the camera
    $scope.turnCameraOff();

    // draw the image into the canvas, take a jpg out of it and upload
    context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight); //, canvas.width, canvas.height);
    canvas.toBlob(function(oMyBlob){

        $scope.uploadFile(oMyBlob);

    }, "image/jpeg", 0.85);

}