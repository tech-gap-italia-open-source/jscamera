jsCamera
========

A library that allows you to show the desktop's webcam or phone camera in your web app.

Please read [some background](doc/existing-api.md) on the existing javascript APIs.

## Deploy as Bower Package

### New Version

Each time you want to release a new version, you have to create a tag and push it to the `origin/master`
in order to let Bower to pick it up:

    git tag x.y.z
    git push --tag

**Please chose the new version tag wisely** as Bower uses [semantic versioning](http://semver.org/).
So for instance remember to increare the *major* if your moification is not at all backward compatible.

Bower should become aware of the new version automatically.



### First Registration

If it's the first time you contribute the package to Bower, please read
<https://bower.io/docs/creating-packages/#register> before proceeding.

If you want to contribute the package please execute:

    bower register jscamera https://gitlab.com/tech-gap-italia-open-source/jscamera.git



### Usage

It should be now enough to include jscamera the usual way, in your `bower.json` file.

    {
        "name": "my-project",
        ...
        "dependencies": {
            "jscamera":"0.0.2"
        }
    }
